package main

import (
	"net/http"
	"github.com/go-redis/redis"
	"github.com/magiconair/properties"
	"bytes"
	"fmt"
	"io/ioutil"
	"time"
	"encoding/json"
)

func initParam()map[string]string{
	p := properties.MustLoadFile("/etc/service.conf", properties.UTF8)
	stan := p.GetString("pos.stan","000002")
	return map[string]string{
		"username": "didy",
		"password": "didy123#",
		"url": "http://36.66.84.226:30345",
		"stan": stan,
	}
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/api/digiroin/biller/inquiry", inquiry)
	mux.HandleFunc("/api/digiroin/biller/payment", payment)
	mux.HandleFunc("/api/digiroin/biller/topup", topup)
	mux.HandleFunc("/api/digiroin/biller/advice", advice)
	http.ListenAndServe(":7064", mux)
}

type Adivice struct {
	ProductCode string
	Number string
	Nominal string
	Reference string
	TimeStamp time.Time
}

type InquiryResponse struct {
	Refbayar string
}


type Payment struct {
	ProductCode string
	Number string
	Nominal string
	Reference string
	TimeStamp time.Time
}

type Inquiry struct {
	ProductCode string
	Number string
	Nominal string
	TimeStamp time.Time
}

type Pulsa struct {
	Phone string
	Type string
	Denom string
	TimeStamp time.Time
}

type PulsaResponse struct {
	RC string `json:"rc"`
	Ket string `json:"ket"`
}

func initRedis() *redis.Client{
	p := properties.MustLoadFile("/etc/service.conf", properties.UTF8)
	address := p.GetString("redis.name","localhost")+":"+p.GetString("redis.port","6379")
	client := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: p.GetString("redis.password",""),
		DB:       p.GetInt("redis.db",0),
	})
	return client
}

func inquiry(w http.ResponseWriter, r *http.Request) {
	reqInquiry := Inquiry{}
	err := json.NewDecoder(r.Body).Decode(&reqInquiry)
	if err != nil{
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
	}else{
		reqInquiry.TimeStamp =time.Now().Local()
		param := initParam()
		client := &http.Client{}
		var jsonprep string = `{"type" : "inquiry","account" : "informasi_mitra","produk" : "`+reqInquiry.ProductCode+`","idpel" : "`+reqInquiry.Number+`","rc" : "","ket" : "","nominal" : "`+reqInquiry.Nominal+`","admin" : "","stan": "000001","retrieval" : "201708151556123","refbayar" : "","timestamp" : "`+reqInquiry.TimeStamp.String()+`","resi" : "","info" : ""}`
		var jsonStr = []byte(jsonprep)
		req, err := http.NewRequest("POST", param["url"], bytes.NewBuffer(jsonStr))
		req.Header.Set("Content-Type", "application/json")
		req.SetBasicAuth(param["username"], param["password"])
		resp, err := client.Do(req)
		if err != nil {
			fmt.Println(err.Error())
		} else {
			body, _ := ioutil.ReadAll(resp.Body)
			w.WriteHeader(http.StatusOK)
			w.Write([]byte(body))
		}
	}
}

func payment(w http.ResponseWriter, r *http.Request) {
	reqPayment := Payment{}
	err := json.NewDecoder(r.Body).Decode(&reqPayment)
	if err != nil{
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
	}else{
		reqPayment.TimeStamp =time.Now().Local()
		param := initParam()
		client := &http.Client{}
		var jsonprep string = `{"type": "payment","account": "informasi_mitra","produk": "`+reqPayment.ProductCode+`","idpel": "`+reqPayment.Number+`","rc": "","ket": "","nominal": "`+reqPayment.Nominal+`","admin": "00001900","stan": "000001","retrieval": "201708151556123","refbayar": "`+reqPayment.Reference+`","timestamp": "`+reqPayment.TimeStamp.String()+`","resi": "","info": ""}`
		var jsonStr = []byte(jsonprep)
		req, err := http.NewRequest("POST", param["url"], bytes.NewBuffer(jsonStr))
		req.Header.Set("Content-Type", "application/json")
		req.SetBasicAuth(param["username"], param["password"])
		resp, err := client.Do(req)
		if err != nil {
			fmt.Println("Unable to reach the server.")
		} else {
			body, _ := ioutil.ReadAll(resp.Body)
			w.WriteHeader(http.StatusOK)
			w.Write([]byte(body))
		}
	}
}

//{
//"Phone":"081213631232",
//"Type":"telkomsel",
//"Denom":"5000"
//}
func topup(w http.ResponseWriter, r *http.Request) {
	reqInquiry := Pulsa{}
	reqInquiryResponse := InquiryResponse{}
	reqPayment := Payment{}
	err := json.NewDecoder(r.Body).Decode(&reqInquiry)
	if err != nil || len(reqInquiry.Phone) <3{
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
	}else{
		lastTwoChar := reqInquiry.Phone[len(reqInquiry.Phone)-(len(reqInquiry.Phone)-2):]
		lastTwoChar = "0"+lastTwoChar
		reqInquiry.TimeStamp =time.Now().Local()
		param := initParam()
		client := &http.Client{}
		redis := initRedis()
		exist := redis.Get("pos:"+reqInquiry.Type+":"+reqInquiry.Denom)
		if(exist.Val()==""){
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(`{"error":"invalid denom"}`))
			return
		}
		var jsonprep string = `{"type" : "inquiry","account" : "informasi_mitra","produk" : "`+exist.Val()+`","idpel" : "`+lastTwoChar+`","rc" : "","ket" : "","nominal" : "`+reqInquiry.Denom+`","admin" : "","stan": "`+param["stan"]+`","retrieval" : "201708151556123","refbayar" : "","timestamp" : "`+reqInquiry.TimeStamp.String()+`","resi" : "","info" : ""}`
		var jsonStr = []byte(jsonprep)
		req, err := http.NewRequest("POST", param["url"], bytes.NewBuffer(jsonStr))
		req.Header.Set("Content-Type", "application/json")
		req.SetBasicAuth(param["username"], param["password"])
		resp, err := client.Do(req)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
		} else {
			err := json.NewDecoder(resp.Body).Decode(&reqInquiryResponse)
			if(err != nil){
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(err.Error()))
			}else{
				reqPayment.Nominal = reqInquiry.Denom
				reqPayment.Reference = reqInquiryResponse.Refbayar
				reqPayment.TimeStamp =time.Now().Local()
				reqPayment.Number = lastTwoChar
				reqPayment.ProductCode = exist.Val()
				var jsonprep string = `{"type": "payment","account": "informasi_mitra","produk": "`+reqPayment.ProductCode+`","idpel": "`+reqPayment.Number+`","rc": "","ket": "","nominal": "`+reqPayment.Nominal+`","admin": "00001900","stan": "`+param["stan"]+`","retrieval": "201708151556123","refbayar": "`+reqPayment.Reference+`","timestamp": "`+reqPayment.TimeStamp.String()+`","resi": "","info": ""}`
				var jsonStr = []byte(jsonprep)
				req, err := http.NewRequest("POST", param["url"], bytes.NewBuffer(jsonStr))
				req.Header.Set("Content-Type", "application/json")
				req.SetBasicAuth(param["username"], param["password"])
				resp, err := client.Do(req)
				if err != nil {
					w.WriteHeader(http.StatusInternalServerError)
					w.Write([]byte(err.Error()))
				} else {
					response := PulsaResponse{}
					err := json.NewDecoder(resp.Body).Decode(&response)
					if(err != nil){
						w.WriteHeader(http.StatusInternalServerError)
						w.Write([]byte(`{"Error":"`+err.Error()+`"}`))
					}else{
						if(response.RC=="P00"){
							w.WriteHeader(http.StatusOK)
							w.Write([]byte(`{"Status":"Payment Success"}`))
						}else{
							w.WriteHeader(http.StatusInternalServerError)
							w.Write([]byte(`{"Error":"`+response.Ket+`"}`))
						}
					}
				}
			}
		}
	}
}

func advice(w http.ResponseWriter, r *http.Request) {
	reqPayment := Payment{}
	err := json.NewDecoder(r.Body).Decode(&reqPayment)
	if err != nil{
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
	}else{
		reqPayment.TimeStamp =time.Now().Local()
		param := initParam()
		client := &http.Client{}
		var jsonprep string = `{"type": "advice","account": "informasi_mitra","produk": "`+reqPayment.ProductCode+`","idpel": "`+reqPayment.Number+`","rc": "","ket": "","nominal": "`+reqPayment.Nominal+`","admin": "00001900","stan": "000001","retrieval": "201708151556123","refbayar": "`+reqPayment.Reference+`","timestamp": "`+reqPayment.TimeStamp.String()+`","resi": "","info": ""}`
		var jsonStr = []byte(jsonprep)
		req, err := http.NewRequest("POST", param["url"], bytes.NewBuffer(jsonStr))
		req.Header.Set("Content-Type", "application/json")
		req.SetBasicAuth(param["username"], param["password"])
		resp, err := client.Do(req)
		if err != nil {
			fmt.Println("Unable to reach the server.")
		} else {
			body, _ := ioutil.ReadAll(resp.Body)
			w.WriteHeader(http.StatusOK)
			w.Write([]byte(body))
		}
	}
}
